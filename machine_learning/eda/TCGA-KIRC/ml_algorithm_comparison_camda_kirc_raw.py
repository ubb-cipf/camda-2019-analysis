#!/usr/bin/env python
# coding: utf-8

# # Machine learning algorithm comparison

# ## Loading libraries and data

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cluster import KMeans
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder


# In[2]:


dataset = pd.read_csv('/home/sromera/cancer-metatest/data/ML_datasets/CAMDA/TCGA-BRCA/kirc_tumor.csv')
X = dataset.iloc[:, :-1].values 
y = dataset.iloc[:, -1].values


# ## Data scaling

# In[3]:


sc = StandardScaler()
X = sc.fit_transform(X)


# ## Label codification

# In[4]:


le = LabelEncoder()
le.fit(y)
list(le.classes_)



# ## Model comparison

# In[6]:


seed = 0 
models = []
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('KMN', KMeans()))
models.append(('RF', RandomForestClassifier()))


# In[7]:


results = []
names = []
scoring = 'accuracy'
for name, model in models:
	kfold = model_selection.KFold(n_splits=10, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X, y, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	# print(msg)


# ## Visualizing model results

# In[8]:


mean = np.mean(results, axis=1)
std = np.std(results, axis=1)
df = pd.DataFrame(
    {'Model_Id': names,
    'Accuracy_mean': mean,
    'Std': std})
df


# In[9]:


fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()

