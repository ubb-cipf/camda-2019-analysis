
library(ggplot2)
library(reshape2)
library(mixOmics)
library(ellipse)
library(RColorBrewer)

dataset <- read.table("~/cancer-metatest/data/ML_datasets/CAMDA/All_tumors/all_tumors_top10.csv", sep = ",", header = TRUE)

variable_matrix <- as.matrix(dataset[,names(dataset) != "labels"])
labels_vector <- dataset[, "labels"]

# Dataset dimensions and classes
str(dataset)

# Statistical summary of variable matrix
summary(as.vector(variable_matrix)) 

# Statistical smummay of each variable values
do.call(cbind, lapply(variable_matrix, summary))

table(labels_vector)

prop.table(table(labels_vector))

boxplot(log(variable_matrix) +(abs(min(variable_matrix))+1))

sample_m <- sample(colnames(variable_matrix), ceiling(ncol(dataset)*0.1))

boxplot(log(as.matrix(variable_matrix[, sample_m]) +(abs(min(variable_matrix[, sample_m]))+1)))

# Assessing variable correlation
cormat <- round(cor(variable_matrix),2)
cormat

my_colors <- brewer.pal(5, "Spectral")
my_colors <- colorRampPalette(my_colors)(100)

ord <- order(cormat[1, ])
data_ord <- cormat[ord, ord]

plotcorr(data_ord , col=my_colors[data_ord*50+50] , mar=c(1,1,1,1)  )

n_variables <- dim(variable_matrix)[2]
cor05 <- sum((cormat > 0.5 | cormat < -0.5) & cormat < 1) / (n_variables*n_variables)
cor07 <- sum((cormat > 0.7 | cormat < -0.7) & cormat < 1) / (n_variables*n_variables)
cor09 <- sum((cormat > 0.9 | cormat < -0.9) & cormat < 1) / (n_variables*n_variables)
data.frame(Cor_0.5 = cor05, Cor_0.7 = cor07, Cor_0.9 = cor09)


# Variable distribution through PCA

tune.pca(variable_matrix, ncomp = 10, center = TRUE, scale = FALSE)

pca <- pca(variable_matrix, ncomp = 3, center = TRUE, scale = FALSE)
pc1 <- pca$x[,1]
pc2 <- pca$x[,2]
pc3 <- pca$x[,3]

df <- data.frame ('PC1' =as.vector(pca$x[,1]),
            'PC2' = as.vector(pca$x[,2]),
            'PC3' = as.vector(pc3 <- pca$x[,3]),
            'Group' = labels_vector)

p <- ggplot(df, aes(x=df$PC1, y=df$PC2))
p <- p + geom_point(aes(color=df$Group))
p <- p + ggtitle("PCA")
p <- p + labs(x="PC1", y="PC2", color="Grupo")
p
