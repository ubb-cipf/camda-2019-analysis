#
# Analisis para el CAMDA 2019
# Comparación Normal - Tumor de miRNA
# Marta Hidalgo, mhidalgo@cipf.es
# 07-05-2019
# 
#

rm(list = ls())


# FOLDERS
#######################

folder <- ""
# setwd(paste0(folder, "saved_data/"))
resfold <- paste0(folder, "results/")


# LIBRARIES
library(hipathia)
library(limma)

# SOURCES
source("scripts_public/100_functions.R")


# ANALYSIS
#######################

projects <- c("TCGA-BRCA", "TCGA-KIRC", "TCGA-LUAD")

for(project in projects){
    
    print(project)
    path <- paste0("saved_data/", project)
    
    # Load Sample_info file
    samfile <- list.files(pattern = "010_Sample_miRNA_info", path = path, full.names = T)
    samf <- load(samfile)
    sam <- get(samf)
    
    # Select samples to compare
    classes <- c("Solid Tissue Normal", "Primary Tumor")
    sams <- rownames(sam)[sam$Sample.Type %in% classes]
    sam <- sam[sams,]
    
    # Comparisons
    # features <- c("paths", "uniprot", "GO")
    # for(feat in features){
    
    # Load Data file
    file <- list.files(pattern = "TMM_miRNA", path = path, full.names = T)
    featdata <- load(file)
    data <- get(featdata)
    
    # Filter selected samples
    data <- data[,sams]
    
    # Filter data with some variance
    data <- data[apply(data, 1, var) > 0.1,]
    
    # w <- do_wilcoxon(data, sam$Sample.Type, "Primary_Tumor", "Solid_Tissue_Normal")
    sam$Sample.Type <- gsub(" ", "_", sam$Sample.Type)
    l <- do_limma(data, sam$Sample.Type, "Primary_Tumor", "Solid_Tissue_Normal", order =T)
    table(l$FDRp.value < 0.05)
    tums <- sam$Sample.ID[sam$Sample.Type == "Primary_Tumor"]
    norms <- sam$Sample.ID[sam$Sample.Type == "Solid_Tissue_Normal"]
    mtum <- apply(assay(data[,tums]), 1, mean)
    mnor <- apply(assay(data[,norms]), 1, mean)
    fc <- log(mtum/mnor, base = 2)
    l$logFC <- fc[rownames(l)]
    table(l$FDRp.value < 0.05 & abs(l$logFC) > 2 & !l$logFC == Inf)
    supersigs <- rownames(l[(l$FDRp.value < 0.05 & abs(l$logFC) > 2),])
    if(length(supersigs) > 1)
        heatmap_plot(data[supersigs,], sam$Sample.Type, legend_xy = "bottomright" , variable_clust = T, legend = F, 
                     save_png = paste0(resfold, project, "/plots/012_Heatmap_plot_miRNA_", project, ".png"))
    
    for(i in rownames(l)[1:15])
        boxplot(data[i,]~sam$Sample.Type, main = i)
    heatmap_plot(data[rownames(l)[1:20],], sam$Sample.Type, legend_xy = "bottomright" , variable_clust = T, legend = F, 
                 save_png = paste0(resfold, project, "/plots/012_Heatmap_plot_miRNA_", project, "_top20.png")) 
    
    # Volcano plot
    png(paste0(resfold, project, "/plots/012_Volcano_plot_miRNA_", project, ".png"))
    plot(l$logFC, -log(l$FDRp.value, base = 10), cex = 0.5, xlab = "log FC", ylab = "-log P.value")
    dev.off()
    
    ## SAVE RESULTS
    write.table(l, file = paste0(resfold, project, "/012_Limma_results_miRNA_", project, ".txt"), 
                sep = "\t", quote = F, row.names = T, col.names = T)
    # }
    

}

